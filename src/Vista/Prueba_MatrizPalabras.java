/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.MatrizPalabra;
import Negocio.Tiempo_Ejecucion;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class Prueba_MatrizPalabras {
    
    public static void main(String[] args) {
        try {
            ArchivoLeerURL archivo=new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt");
            
            Object lineas[]=archivo.leerArchivo();
            
            int canLineas = 30000;//PONER LA CANTIDAD DE DATOS A PROCESAR
            MatrizPalabra m=new MatrizPalabra(lineas,canLineas);    
                                
//            Experimento_1000_Normal(m);
//            Experimento_1000_Mejorado(m);
//            
//            Experimento_5000_Normal(m);
//            Experimento_5000_Mejorado(m);
//            
//            Experimento_10000_Normal(m);
//            Experimento_10000_Mejorado(m);
//            
//            Experimento_20000_Normal(m);
//            Experimento_20000_Mejorado(m);
//            
//            Experimento_30000_Normal(m);
//            Experimento_30000_Mejorado(m);
//            
//            Experimento_80000_Normal(m);
//            Experimento_80000_Mejorado(m);
            
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
         
    }
    
    public static void Experimento_1000_Normal(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbuja();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    public static void Experimento_1000_Mejorado(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbujaMejorado();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    
    public static void Experimento_5000_Normal(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbuja();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    public static void Experimento_5000_Mejorado(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbujaMejorado();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    
    public static void Experimento_10000_Normal(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbuja();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    public static void Experimento_10000_Mejorado(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbujaMejorado();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    
    public static void Experimento_20000_Normal(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbuja();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    public static void Experimento_20000_Mejorado(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbujaMejorado();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    
    public static void Experimento_30000_Normal(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbuja();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    public static void Experimento_30000_Mejorado(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbujaMejorado();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    
    public static void Experimento_80000_Normal(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbuja();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
    public static void Experimento_80000_Mejorado(MatrizPalabra m){
    
        Tiempo_Ejecucion TE = new Tiempo_Ejecucion();
        m.OrdenarBurbujaMejorado();
        TE.stop();
        System.out.println("Tardo "+TE.getTiempoMillis()+" Milisegundos");
    }
}
